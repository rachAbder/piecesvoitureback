package com.formation.projetpiecesvoitureback.model.pieces;

import com.formation.projetpiecesvoitureback.model.Piece;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Moteur")
public class Moteur extends Piece {
}

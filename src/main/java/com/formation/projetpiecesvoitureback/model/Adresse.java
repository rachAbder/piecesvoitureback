package com.formation.projetpiecesvoitureback.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class Adresse implements Serializable {

	private Integer numero;
	private String rue;
	
	@Column(name="CODE_POSTALE")
	private Integer codePostale;
	private String ville;
	public Adresse(Integer numero, String rue, Integer codePostale, String ville) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.codePostale = codePostale;
		this.ville = ville;
	}
	
	
	
	
}

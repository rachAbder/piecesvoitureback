package com.formation.projetpiecesvoitureback.repository;
import com.formation.projetpiecesvoitureback.model.Commande;

import org.springframework.data.jpa.repository.JpaRepository;



import java.util.List;

public interface CommandeRepository extends JpaRepository<Commande, String>{

    //List<Commande> findAllByClient(Client client);
}

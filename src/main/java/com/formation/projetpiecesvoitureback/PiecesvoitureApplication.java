package com.formation.projetpiecesvoitureback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiecesvoitureApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiecesvoitureApplication.class, args);
	}

}

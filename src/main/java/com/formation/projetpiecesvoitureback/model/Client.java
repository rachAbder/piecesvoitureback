package com.formation.projetpiecesvoitureback.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;



import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.lang.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Client implements Serializable {

	@ToString.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private int id;
	
	@ToString.Include
	@NaturalId
	private String email;
	@ToString.Include
	private String nom;
	@ToString.Include
	private String prenom;

	@ToString.Include
	private String password;

	@ToString.Exclude
	@OneToOne(mappedBy="client", fetch=FetchType.LAZY)
	private Panier panier;

	@ToString.Include
	@Embedded
	@Nullable
	private Adresse adresse;
	
	@ToString.Include
	@Version
	@Column(nullable = false)
	private int version;

	
	@ToString.Exclude
	@OneToMany(mappedBy="client")
	private List<Commande> commandes;

	public Client( String email, String nom, String prenom, String password) {
		
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;

	}
}

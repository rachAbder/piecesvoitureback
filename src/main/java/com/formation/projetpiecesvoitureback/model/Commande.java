package com.formation.projetpiecesvoitureback.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Commande implements Serializable {

	@Id
	@JsonView(JsonViews.Common.class)
	private String numeroCommande;
	
	@JoinColumn(name="client_id")
	@ManyToOne
	@JsonView(JsonViews.Common.class)
	private Client client;

	@JsonView(JsonViews.CommandeWithPiece.class)
	@JoinTable(
	name = "ligne_commande",
	joinColumns = @JoinColumn(name = "commande_id"),
	inverseJoinColumns = @JoinColumn(name = "piece_id"))
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Piece> pieces;
}

package com.formation.projetpiecesvoitureback.repository;

import com.formation.projetpiecesvoitureback.model.Panier;
import org.springframework.data.jpa.repository.JpaRepository;



public interface PanierRepository extends JpaRepository<Panier, String>{

}

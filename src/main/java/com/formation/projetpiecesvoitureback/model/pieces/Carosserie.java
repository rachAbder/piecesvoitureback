package com.formation.projetpiecesvoitureback.model.pieces;



import com.formation.projetpiecesvoitureback.model.Piece;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Carosserie")
public class Carosserie extends Piece {
}

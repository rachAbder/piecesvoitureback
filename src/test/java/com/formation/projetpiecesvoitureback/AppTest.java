package com.formation.projetpiecesvoitureback;

import com.formation.projetpiecesvoitureback.model.Client;
import com.formation.projetpiecesvoitureback.model.Panier;
import com.formation.projetpiecesvoitureback.model.Piece;
import com.formation.projetpiecesvoitureback.model.pieces.Frein;
import com.formation.projetpiecesvoitureback.model.pieces.Moteur;
import com.formation.projetpiecesvoitureback.repository.ClientRepository;
import com.formation.projetpiecesvoitureback.repository.PanierRepository;
import com.formation.projetpiecesvoitureback.repository.PieceRepository;



import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {

	public static void main(String[] args) {
		testfindPiece() ;
	}

	public static void testFindAll() {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("./applicationContext.xml");
		ClientRepository repo = ctx.getBean(ClientRepository.class);
		List<Client> lst = repo.findAll();
		lst.forEach(client -> System.out.println(client.toString()));

		ctx.close();
	}

	public static void testCreateClient() {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("./applicationContext.xml");
		ClientRepository repo = ctx.getBean(ClientRepository.class);
		Client c = new Client("rach@dff.com", "abd", "fef", "fdfr55ggG@");
		System.out.println();
		// Adresse adr = new Adresse(10, "reei", 45222, "ujiushdif");
		repo.save(c);

		ctx.close();
	}

	public static void testCreatePanier() {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("./applicationContext.xml");
		PieceRepository piecRepo = ctx.getBean(PieceRepository.class);
		PanierRepository pRepo = ctx.getBean(PanierRepository.class);
		System.out.println();

		Panier pa = new Panier();
		pa.setId("p1");
		pa.getLstPiece().addAll(piecRepo.findAll());
		pRepo.save(pa);

		ctx.close();
	}

	public static void testCreatePiece() {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("./applicationContext.xml");
		PieceRepository piecRepo = ctx.getBean(PieceRepository.class);

		Piece piece = new Moteur();
		piece.setRef("p1x45");
		piecRepo.save(piece);

		Piece frein = new Frein();
		frein.setRef("p2");
		piecRepo.save(frein);
		ctx.close();
	}

	public static void testfindPiece() {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("./applicationContext.xml");
		PieceRepository piecRepo = ctx.getBean(PieceRepository.class);

		Piece piece = piecRepo.findById("p2").get();
		System.out.println("les commandes");
		piece.getCommandes().forEach(commande -> System.out.println(commande));
		ctx.close();
	}
}

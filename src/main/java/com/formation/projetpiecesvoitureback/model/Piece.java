package com.formation.projetpiecesvoitureback.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Type_piece",discriminatorType = DiscriminatorType.STRING)
@Entity
public class Piece implements Serializable {
	
	@Id
	@JsonView(JsonViews.Common.class)
	private String ref;
	@JsonView(JsonViews.Common.class)
	private String nom;
	@JsonView(JsonViews.Common.class)
	private String marque;
	@JsonView(JsonViews.Common.class)
	private String description;
	@JsonView(JsonViews.Common.class)
	private double tarif;
	
	
	@ManyToMany(mappedBy= "pieces", fetch = FetchType.EAGER)
	@JsonView(JsonViews.PieceWithCommande.class)
	private List<Commande> commandes;
	


	public Piece(String ref, String nom, String description, double tarif) {
		super();
		this.ref = ref;
		this.nom = nom;
		this.description = description;
		this.tarif = tarif;

	}


	@Override
	public String toString() {
		return "Piece{" +
				"ref='" + ref + '\'' +
				", nom='" + nom + '\'' +
				", description='" + description + '\'' +
				", tarif=" + tarif +
				'}';
	}
}

package com.formation.projetpiecesvoitureback.repository;

import com.formation.projetpiecesvoitureback.model.Client;

import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Optional;


public interface ClientRepository extends JpaRepository<Client, Integer>{
    Optional<Client> findByEmailAndPassword(String email, String password);

}

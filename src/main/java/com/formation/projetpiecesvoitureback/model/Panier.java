package com.formation.projetpiecesvoitureback.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Getter
@Setter
@NoArgsConstructor
@EnableTransactionManagement
@Entity
public class Panier implements Serializable {
	
	@Id
	private String id;
	
	
	@JoinColumn(name="client_id")
	@OneToOne(fetch=FetchType.LAZY)
	private Client client;
	
	@OneToMany
    @JoinColumn(name = "panier_id")
    private List<Piece> lstPiece;

    private double prixTotal = 0;

    public void setLstPiece(List<Piece> lstPiece){
        prixTotal = 0;
        for(Piece a : lstPiece)
            prixTotal += a.getTarif();
        this.lstPiece = lstPiece;
    }
}

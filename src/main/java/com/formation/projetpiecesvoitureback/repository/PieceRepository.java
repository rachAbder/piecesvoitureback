package com.formation.projetpiecesvoitureback.repository;


import com.formation.projetpiecesvoitureback.model.Piece;
import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.stereotype.Repository;


@Repository
public interface PieceRepository extends JpaRepository<Piece, String>{
    //public List<Piece> findByTypeEquals(TypeArticle type);
}
